window.addEventListener('load', function() {
    const loadingScreen = document.getElementById('loadingScreen');
    loadingScreen.style.display = 'none';
});
function toggleMenu() {
    var navList = document.getElementById('nav-list');
    navList.classList.toggle('show');
}



// Cuando el usuario hace clic en cualquier lugar fuera del modal, ciérralo
window.onclick = function(event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}


document.addEventListener('DOMContentLoaded', function () {
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("toggleModal");
    btn.onclick = function() {
        
        var isOpen = modal.style.display === "block";
        
        // Cambia el estado de visualización del modal
        modal.style.display = isOpen ? "none" : "block";
        
        // Actualiza el texto del botón
        btn.textContent = isOpen ? "☰ Conócenos!" : "X Conócenos!";
    }

    // Opcional: Cierra el modal cuando se hace clic en el área fuera del modal-content
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
            btn.textContent = "☰ Conócenos!"; // Restablece el texto del botón
        }
    }
});


const frames = []; // Aquí irían las imágenes de cada cuadro de la animación
const container = document.getElementById('animation-container');
let currentFrame = 0;
const basePath = 'src/';
// Simula la carga de imágenes
for (let i = 1; i < 5; i++) {
  let img = new Image();
  img.src = `${basePath}image_${i}.jpeg`;  // Asume que tienes imágenes nombradas secuencialmente
  img.classList.add('frame');
  img.style.display = 'none'; // Oculta la imagen por defecto
  frames.push(img);
  container.appendChild(img);
}

function animateFrames() {
  frames[currentFrame].style.display = 'none';
  currentFrame = (currentFrame + 1) % frames.length;
  frames[currentFrame].style.display = 'block';
}

// Cambia el cuadro cada 1 segundo para simular una animación GIF
setInterval(animateFrames, 2500);


function ajustarTextoSegunAncho() {
    var anchoPantalla = window.innerWidth;
    var labelAnio = document.getElementById('anioslabel');
    var clientesAtendidos = document.getElementById('clientesatendidos');
    var productospropios = document.getElementById('productospropios');
  
    if (anchoPantalla > 300 && anchoPantalla < 1000) {
      // Cambia el texto si el ancho está entre 601px y 1200px
      labelAnio.textContent = 'Años';
      clientesAtendidos.textContent = 'Clientes';
      productospropios.textContent = 'Productos';
    } else {
      // Texto original o adecuado para otros tamaños de pantalla
      labelAnio.textContent = 'Años de experiencia';
      clientesAtendidos.textContent = 'Clientes atendidos';
      productospropios.textContent = 'Productos propios';
    }
  }
  
  // Ejecuta la función al cargar la página
  ajustarTextoSegunAncho();
  
  // Añade un evento 'resize' para ajustar el texto cuando se cambia el tamaño de la ventana
  window.addEventListener('resize', ajustarTextoSegunAncho);